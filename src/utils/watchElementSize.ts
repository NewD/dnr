export const watchElementSize = (
  element: HTMLElement,
  onChange: (size: [number, number]) => void
) => {
  const resizeObserver = new ResizeObserver((entries) => {
    for (const entry of entries) {
      if (entry.contentBoxSize) {
        // Firefox implements `contentBoxSize` as a single content rect, rather than an array
        const contentBoxSize: { inlineSize: number; blockSize: number } = Array.isArray(
          entry.contentBoxSize
        )
          ? entry.contentBoxSize[0]
          : entry.contentBoxSize;

        onChange([contentBoxSize.inlineSize, contentBoxSize.blockSize]);
      }
    }
  });

  resizeObserver.unobserve(element);
  resizeObserver.observe(element);

  return resizeObserver;
};
