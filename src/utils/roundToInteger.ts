type Limit = { min: number } | { max: number };

export const roundToInteger = (initNumber: number, target: number, limits?: Limit): number => {
  const modValue = initNumber % target;

  if (target === 1 || modValue === 0) {
    return initNumber;
  }

  if (modValue > target / 2) {
    let result = initNumber + target - modValue;
    if (!limits) {
      return result;
    }

    if ('max' in limits) {
      result =
        result < limits.max + 1 ? result : roundToInteger(limits.max, target, { max: limits.max });
    }

    if ('min' in limits) {
      result =
        result > limits.min - 1 ? result : roundToInteger(limits.min, target, { min: limits.min });
    }
    return result;
  }

  let result = initNumber - modValue;
  if (!limits) {
    return result;
  }

  if ('max' in limits) {
    result = result < limits.max ? result : roundToInteger(limits.max, target, { max: limits.max });
  }

  if ('min' in limits) {
    result = result > limits.min ? result : roundToInteger(limits.min, target, { min: limits.min });
  }

  return result;
};
