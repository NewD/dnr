export const min = (a: number, b: number) => (a <= b ? a : b);
