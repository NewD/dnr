export const max = (a: number, b: number) => (a >= b ? a : b);
