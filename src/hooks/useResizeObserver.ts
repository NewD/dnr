import { useEffect, useState } from 'react';

import { watchElementSize } from '../utils/watchElementSize';

export const useResizeObserver = (setSize: (size: [number, number]) => void) => {
  const [elementRef, setElementRef] = useState<HTMLElement | null>(null);

  useEffect(() => {
    if (!elementRef) {
      return;
    }

    const resizeObserver = watchElementSize(elementRef, setSize);

    return () => resizeObserver.disconnect();
  }, [elementRef, setSize]);

  return [elementRef, setElementRef] as const;
};
