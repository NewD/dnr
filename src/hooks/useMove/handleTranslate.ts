import type { MutableRefObject } from 'react';

import type { OnMoveParams } from '../useDnR';
import { transformTranslateRegExp } from '../useDnR';

interface HandleTranslateParams {
  event: MouseEvent | Touch;
  moveEvent: MouseEvent | TouchEvent;
  element: HTMLElement | SVGElement;
  parentWidth: MutableRefObject<number>;
  parentHeight: MutableRefObject<number>;
  initWidth: MutableRefObject<number>;
  initHeight: MutableRefObject<number>;
  isMobile: boolean;
  isDragging: MutableRefObject<boolean>;
  translateInitX: MutableRefObject<number>;
  translateInitY: MutableRefObject<number>;
  topOffset: MutableRefObject<number>;
  bottomOffset: MutableRefObject<number>;
  leftOffset: MutableRefObject<number>;
  rightOffset: MutableRefObject<number>;
  hasLeftDirection: MutableRefObject<boolean>;
  hasRightDirection: MutableRefObject<boolean>;
  translateRectX: MutableRefObject<number>;
  translateRectY: MutableRefObject<number>;
  shiftX: MutableRefObject<number>;
  shiftY: MutableRefObject<number>;
  prevTransformTranslateX: MutableRefObject<number>;
  prevTransformTranslateY: MutableRefObject<number>;
  hasTopDirection: MutableRefObject<boolean>;
  hasBottomDirection: MutableRefObject<boolean>;
  containerRef: MutableRefObject<HTMLElement | null>;
  onMoving: MutableRefObject<((params: OnMoveParams) => boolean) | undefined>;
}

export const handleTranslate = (params: HandleTranslateParams) => {
  const {
    event,
    moveEvent,
    element,
    translateInitX,
    translateInitY,
    bottomOffset,
    containerRef,
    hasBottomDirection,
    hasLeftDirection,
    hasRightDirection,
    hasTopDirection,
    initHeight,
    initWidth,
    isDragging,
    isMobile,
    leftOffset,
    parentHeight,
    parentWidth,
    prevTransformTranslateX,
    prevTransformTranslateY,
    rightOffset,
    shiftX,
    shiftY,
    topOffset,
    translateRectX,
    translateRectY,
    onMoving,
  } = params;

  let transformTranslateX = Number(translateInitX.current);
  let transformTranslateY = Number(translateInitY.current);

  switch (true) {
    case hasLeftDirection.current && hasRightDirection.current: {
      transformTranslateX =
        Number(translateInitX.current) + event.clientX - translateRectX.current - shiftX.current;

      if (transformTranslateX < leftOffset.current) {
        transformTranslateX = leftOffset.current;
      }
      if (transformTranslateX >= parentWidth.current - initWidth.current - leftOffset.current) {
        transformTranslateX = parentWidth.current - initWidth.current - leftOffset.current;
      }

      break;
    }
    case hasLeftDirection.current: {
      const newTransformTranslateX =
        Number(translateInitX.current) + event.clientX - translateRectX.current - shiftX.current;

      if (newTransformTranslateX > prevTransformTranslateX.current) {
        transformTranslateX = prevTransformTranslateX.current;
        break;
      }

      transformTranslateX = newTransformTranslateX;

      if (transformTranslateX < leftOffset.current) {
        transformTranslateX = leftOffset.current;
      }

      prevTransformTranslateX.current = transformTranslateX;

      break;
    }
    case hasRightDirection.current: {
      const newTransformTranslateX =
        Number(translateInitX.current) + event.clientX - translateRectX.current - shiftX.current;

      if (newTransformTranslateX < prevTransformTranslateX.current) {
        transformTranslateX = prevTransformTranslateX.current;
        break;
      }

      transformTranslateX = newTransformTranslateX;

      const size = Math.min(initWidth.current, initHeight.current);
      if (transformTranslateX > parentWidth.current - rightOffset.current - size) {
        transformTranslateX = parentWidth.current - rightOffset.current - size;
      }

      prevTransformTranslateX.current = transformTranslateX;
    }
  }

  switch (true) {
    case hasTopDirection.current && hasBottomDirection.current: {
      transformTranslateY =
        Number(translateInitY.current) + event.clientY - translateRectY.current - shiftY.current;

      if (transformTranslateY < topOffset.current) {
        transformTranslateY = topOffset.current;
      }
      if (transformTranslateY >= parentHeight.current - initHeight.current - topOffset.current) {
        transformTranslateY = parentHeight.current - initHeight.current - topOffset.current;
      }

      break;
    }
    case hasTopDirection.current: {
      const newTransformTranslateY =
        Number(translateInitY.current) + event.clientY - translateRectY.current - shiftY.current;

      if (newTransformTranslateY > prevTransformTranslateY.current) {
        transformTranslateY = prevTransformTranslateY.current;
        break;
      }

      transformTranslateY = newTransformTranslateY;

      if (transformTranslateY < topOffset.current) {
        transformTranslateY = topOffset.current;
      }

      prevTransformTranslateY.current = transformTranslateY;

      break;
    }
    case hasBottomDirection.current: {
      const newTransformTranslateY =
        Number(translateInitY.current) + event.clientY - translateRectY.current - shiftY.current;

      if (newTransformTranslateY < prevTransformTranslateY.current) {
        transformTranslateY = prevTransformTranslateY.current;
        break;
      }

      transformTranslateY = newTransformTranslateY;

      if (transformTranslateY > parentHeight.current - bottomOffset.current - initHeight.current) {
        transformTranslateY = parentHeight.current - bottomOffset.current - initHeight.current;
      }

      prevTransformTranslateY.current = transformTranslateY;
    }
  }

  if (
    !onMoving.current?.({
      movingCoords: {
        top: transformTranslateY,
        bottom: transformTranslateY + initHeight.current,
        left: transformTranslateX,
        right: transformTranslateX + initWidth.current,
      },
      draggableElement: element,
      containerElement: containerRef.current,
      isDragging: isDragging.current,
      event: moveEvent,
      isMobile,
    })
  ) {
    if (/translate/.test(element.style.transform || '')) {
      element.style.transform = element.style.transform.replace(
        transformTranslateRegExp,
        `translate(${transformTranslateX}px, ${transformTranslateY}px)`
      );
    } else {
      element.style.transform = `translate(${transformTranslateX}px, ${transformTranslateY}px)`;
    }
  }
};
