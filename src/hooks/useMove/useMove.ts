import type { MutableRefObject } from 'react';

import type { DragMode, OnMoveParams } from '../useDnR';
import { handleTranslate } from './handleTranslate';

interface UseMoveParams {
  parentWidth: MutableRefObject<number>;
  parentHeight: MutableRefObject<number>;
  initWidth: MutableRefObject<number>;
  initHeight: MutableRefObject<number>;
  element: HTMLElement | SVGElement | null;
  isMobile: boolean;
  isDragging: MutableRefObject<boolean>;
  mode: MutableRefObject<DragMode>;
  translateInitX: MutableRefObject<number>;
  translateInitY: MutableRefObject<number>;
  topOffset: MutableRefObject<number>;
  bottomOffset: MutableRefObject<number>;
  leftOffset: MutableRefObject<number>;
  rightOffset: MutableRefObject<number>;
  hasLeftDirection: MutableRefObject<boolean>;
  hasRightDirection: MutableRefObject<boolean>;
  translateRectX: MutableRefObject<number>;
  translateRectY: MutableRefObject<number>;
  shiftX: MutableRefObject<number>;
  shiftY: MutableRefObject<number>;
  prevTransformTranslateX: MutableRefObject<number>;
  prevTransformTranslateY: MutableRefObject<number>;
  hasTopDirection: MutableRefObject<boolean>;
  hasBottomDirection: MutableRefObject<boolean>;
  onMoving: MutableRefObject<((params: OnMoveParams) => boolean) | undefined>;
  right: MutableRefObject<number>;
  left: MutableRefObject<number>;
  top: MutableRefObject<number>;
  bottom: MutableRefObject<number>;
  containerRef: MutableRefObject<HTMLElement | null>;
  minHeight: MutableRefObject<number>;
  minWidth: MutableRefObject<number>;
}

export const useMove = (params: UseMoveParams) => {
  const {
    element,
    parentWidth,
    parentHeight,
    initWidth,
    initHeight,
    isMobile,
    bottomOffset,
    isDragging,
    leftOffset,
    mode,
    rightOffset,
    topOffset,
    translateInitX,
    translateInitY,
    hasLeftDirection,
    hasRightDirection,
    shiftX,
    translateRectX,
    shiftY,
    translateRectY,
    bottom,
    hasBottomDirection,
    hasTopDirection,
    left,
    onMoving,
    prevTransformTranslateX,
    prevTransformTranslateY,
    right,
    top,
    containerRef,
    minHeight,
    minWidth,
  } = params;

  return (moveEvent: MouseEvent | TouchEvent) => {
    if (
      !element ||
      parentWidth.current === 0 ||
      parentHeight.current === 0 ||
      initWidth.current === 0 ||
      initHeight.current === 0
    ) {
      return;
    }

    const event = isMobile ? (moveEvent as TouchEvent).targetTouches[0] : (moveEvent as MouseEvent);
    isDragging.current = true;

    if (mode.current === 'translate') {
      handleTranslate({
        bottomOffset,
        containerRef,
        event,
        moveEvent,
        element,
        onMoving,
        hasBottomDirection,
        hasLeftDirection,
        hasRightDirection,
        hasTopDirection,
        initHeight,
        initWidth,
        isDragging,
        isMobile,
        leftOffset,
        parentHeight,
        parentWidth,
        prevTransformTranslateX,
        prevTransformTranslateY,
        rightOffset,
        shiftX,
        shiftY,
        topOffset,
        translateInitX,
        translateInitY,
        translateRectX,
        translateRectY,
      });

      return;
    }

    // mode === 'move'
    const rect = element.getBoundingClientRect();
    if (initHeight.current < 0) {
      initHeight.current = rect.height;
    }
    if (initWidth.current < 0) {
      initWidth.current = rect.width;
    }

    const offsetTop = 'offsetTop' in element ? element.offsetTop : rect.top;
    const cursorTopOffset = event.clientY - (rect.top - offsetTop);

    // Move vertical
    if (mode.current === 'move') {
      switch (true) {
        // move up and down
        case hasTopDirection.current && hasBottomDirection.current: {
          top.current = cursorTopOffset - shiftY.current;

          if (top.current < topOffset.current) {
            top.current = topOffset.current;
          }
          if (top.current >= parentHeight.current - initHeight.current - bottomOffset.current) {
            top.current = parentHeight.current - initHeight.current - bottomOffset.current;
          }

          bottom.current = parentHeight.current - rect.height - top.current;

          break;
        }
        // move up
        case hasTopDirection.current: {
          const newTop = cursorTopOffset - shiftY.current;

          if (newTop > top.current) {
            break;
          }

          top.current = newTop;

          if (top.current < topOffset.current) {
            top.current = topOffset.current;
          }

          bottom.current = parentHeight.current - rect.height - top.current;
          break;
        }
        case hasBottomDirection.current: {
          const newBottom = parentHeight.current - cursorTopOffset;

          if (newBottom > bottom.current) {
            break;
          }

          bottom.current = newBottom;

          if (bottom.current < bottomOffset.current) {
            bottom.current = bottomOffset.current;
          }
          if (
            bottom.current >
            parentHeight.current - offsetTop - minHeight.current - bottomOffset.current
          ) {
            bottom.current =
              parentHeight.current - offsetTop - minHeight.current - bottomOffset.current;
          }

          top.current = parentHeight.current - rect.height - bottom.current;
        }
      }
    }

    // Resize vertical
    if (mode.current === 'resize') {
      switch (true) {
        case hasTopDirection.current: {
          top.current = cursorTopOffset - shiftY.current;

          if (top.current < topOffset.current) {
            top.current = topOffset.current;
          }
          if (top.current > parentHeight.current - bottom.current - minHeight.current) {
            top.current = parentHeight.current - bottom.current - minHeight.current;
          }

          break;
        }
        case hasBottomDirection.current: {
          bottom.current = parentHeight.current - cursorTopOffset;

          if (bottom.current < bottomOffset.current) {
            bottom.current = bottomOffset.current;
          }
          if (bottom.current > parentHeight.current - offsetTop - minHeight.current) {
            bottom.current = parentHeight.current - offsetTop - minHeight.current;
          }
        }
      }
    }

    const offsetLeft = 'offsetLeft' in element ? element.offsetLeft : rect.left;
    const cursorLeftOffset = event.clientX - (rect.left - offsetLeft);

    // Move horizontal
    if (mode.current === 'move') {
      switch (true) {
        case hasLeftDirection.current && hasRightDirection.current: {
          left.current = cursorLeftOffset - shiftX.current;

          if (left.current < leftOffset.current) {
            left.current = leftOffset.current;
          }
          if (left.current >= parentWidth.current - initWidth.current - leftOffset.current) {
            left.current = parentWidth.current - initWidth.current - leftOffset.current;
          }

          right.current = parentWidth.current - rect.width - left.current;

          break;
        }
        case hasLeftDirection.current: {
          const newLeft = cursorLeftOffset - shiftX.current;

          if (newLeft > left.current) {
            break;
          }

          left.current = newLeft;

          if (left.current < leftOffset.current) {
            left.current = leftOffset.current;
          }
          if (
            left.current >
            parentWidth.current - right.current - minWidth.current - leftOffset.current
          ) {
            left.current =
              parentWidth.current - right.current - minWidth.current - leftOffset.current;
          }

          right.current = parentWidth.current - rect.width - left.current;
          break;
        }
        case hasRightDirection.current: {
          const newRight = parentWidth.current - cursorLeftOffset;

          if (newRight > right.current) {
            break;
          }

          right.current = newRight;

          if (right.current < rightOffset.current) {
            right.current = rightOffset.current;
          }
          if (
            right.current >
            parentWidth.current - offsetLeft - minWidth.current - rightOffset.current
          ) {
            right.current =
              parentWidth.current - offsetLeft - minWidth.current - rightOffset.current;
          }

          left.current = parentWidth.current - rect.width - right.current;
        }
      }
    }

    // Resize horizontal
    if (mode.current === 'resize') {
      switch (true) {
        case hasLeftDirection.current: {
          left.current = cursorLeftOffset - shiftX.current;

          if (left.current < leftOffset.current) {
            left.current = leftOffset.current;
          }
          if (left.current > parentWidth.current - right.current - minWidth.current) {
            left.current = parentWidth.current - right.current - minWidth.current;
          }

          break;
        }
        case hasRightDirection.current: {
          right.current = parentWidth.current - cursorLeftOffset;

          if (right.current < rightOffset.current) {
            right.current = rightOffset.current;
          }
          if (right.current > parentWidth.current - offsetLeft - minWidth.current) {
            right.current = parentWidth.current - offsetLeft - minWidth.current;
          }
        }
      }
    }

    if (
      !onMoving.current?.({
        movingCoords: {
          top: top.current,
          bottom: bottom.current,
          left: left.current,
          right: right.current,
        },
        draggableElement: element,
        containerElement: containerRef.current,
        isDragging: isDragging.current,
        event: moveEvent,
        isMobile,
      })
    ) {
      const prevDisplay = element.style.display;
      element.style.display = 'none';
      if (hasTopDirection.current || (hasBottomDirection && mode.current === 'move')) {
        element.style.top = `${top.current}px`;
      }
      if (hasBottomDirection.current || (hasTopDirection && mode.current === 'move')) {
        element.style.bottom = `${bottom.current}px`;
      }
      if (hasLeftDirection.current || (hasRightDirection && mode.current === 'move')) {
        element.style.left = `${left.current}px`;
      }
      if (hasRightDirection.current || (hasLeftDirection && mode.current === 'move')) {
        element.style.right = `${right.current}px`;
      }

      element.style.display = prevDisplay;
    }
  };
};
