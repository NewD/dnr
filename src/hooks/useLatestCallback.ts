import { useEffect, useRef } from 'react';

export const useLatestCallback = <Callback extends (...args: any[]) => any>(
  cb: Callback
): Callback => {
  const cbRef = useRef(cb);
  useEffect(() => {
    cbRef.current = cb;
  });

  return useRef(((...args) => {
    return cbRef.current(...args);
  }) as Callback).current;
};
