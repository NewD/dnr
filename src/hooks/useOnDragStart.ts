import type { MutableRefObject } from 'react';

import { useLatestCallback } from './useLatestCallback';
import { roundToInteger } from '../utils/roundToInteger';
import type { DragMode, ElementPosition, OnMoveParams } from './useDnR';
import { multiNumberRegExp, transformTranslateRegExp } from './useDnR';
import { min } from '../utils/min';
import { watchElementSize } from '../utils/watchElementSize';

interface UseOnDragStartParams {
  parentWidth: MutableRefObject<number>;
  parentHeight: MutableRefObject<number>;
  initWidth: MutableRefObject<number>;
  initHeight: MutableRefObject<number>;
  element: HTMLElement | SVGElement | null;
  isMobile: boolean;
  isDragging: MutableRefObject<boolean>;
  mode: MutableRefObject<DragMode>;
  translateInitX: MutableRefObject<number>;
  translateInitY: MutableRefObject<number>;
  topOffset: MutableRefObject<number>;
  bottomOffset: MutableRefObject<number>;
  leftOffset: MutableRefObject<number>;
  rightOffset: MutableRefObject<number>;
  translateRectX: MutableRefObject<number>;
  translateRectY: MutableRefObject<number>;
  shiftX: MutableRefObject<number>;
  shiftY: MutableRefObject<number>;
  prevTransformTranslateX: MutableRefObject<number>;
  prevTransformTranslateY: MutableRefObject<number>;
  onMoveStart: MutableRefObject<((params: OnMoveParams) => boolean) | undefined>;
  onMoveEnd: MutableRefObject<((params: OnMoveParams) => boolean) | undefined>;
  right: MutableRefObject<number>;
  left: MutableRefObject<number>;
  top: MutableRefObject<number>;
  bottom: MutableRefObject<number>;
  containerRef: MutableRefObject<HTMLElement | null>;
  gridCellWidth: MutableRefObject<number>;
  gridCellHeight: MutableRefObject<number>;
  gridCellsCountX: MutableRefObject<number>;
  gridCellsCountY: MutableRefObject<number>;
  position: MutableRefObject<ElementPosition>;
  containerElement?: HTMLElement | null;
  shouldPreventOnClick: MutableRefObject<boolean>;
  sholdResizeByVerticalOnMoveEnd: MutableRefObject<boolean>;
  sholdResizeByHorizontalOnMoveEnd: MutableRefObject<boolean>;
  move: (moveEvent: MouseEvent | TouchEvent) => void;
}

export const useOnDragStart = (params: UseOnDragStartParams) => {
  const {
    element,
    parentWidth,
    parentHeight,
    initWidth,
    initHeight,
    isMobile,
    bottomOffset,
    isDragging,
    leftOffset,
    mode,
    rightOffset,
    topOffset,
    translateInitX,
    translateInitY,
    shiftX,
    translateRectX,
    shiftY,
    translateRectY,
    bottom,
    left,
    prevTransformTranslateX,
    prevTransformTranslateY,
    right,
    top,
    containerRef,
    gridCellHeight,
    gridCellWidth,
    gridCellsCountX,
    gridCellsCountY,
    position,
    onMoveEnd,
    onMoveStart,
    containerElement,
    shouldPreventOnClick,
    sholdResizeByHorizontalOnMoveEnd,
    sholdResizeByVerticalOnMoveEnd,
    move,
  } = params;
  return useLatestCallback((moveStartEvent: MouseEvent | TouchEvent) => {
    moveStartEvent.stopPropagation();
    const event = isMobile
      ? (moveStartEvent as TouchEvent).targetTouches[0]
      : (moveStartEvent as MouseEvent);

    if (!element) {
      return;
    }

    element.style.position = position.current;

    const container = containerElement || element.parentElement;
    if (!container && position.current !== 'fixed') {
      return;
    }
    containerRef.current = container;

    let resizeObserver: ResizeObserver | null = null;

    if (position.current === 'absolute') {
      if (!container) {
        return;
      }

      resizeObserver = watchElementSize(container, ([width, height]) => {
        parentWidth.current = Math.ceil(width);
        parentHeight.current = Math.ceil(height);

        gridCellWidth.current =
          gridCellsCountX.current > 0 ? parentWidth.current / gridCellsCountX.current : 1;
        gridCellHeight.current =
          gridCellsCountY.current > 0 ? parentHeight.current / gridCellsCountY.current : 1;
      });

      const containerPosition = getComputedStyle(container).position;
      if (containerPosition !== 'relative' && containerPosition !== 'absolute') {
        container.style.position = 'relative';
      }
    }

    const elementRect = element.getBoundingClientRect();

    if (
      onMoveStart.current?.({
        containerElement: containerRef.current,
        draggableElement: element,
        movingCoords: {
          top: elementRect.top,
          bottom: elementRect.bottom,
          left: elementRect.left,
          right: elementRect.right,
        },
        isDragging: isDragging.current,
        isMobile,
        event: moveStartEvent,
      })
    ) {
      return;
    }

    shiftY.current = event.clientY - elementRect.top;
    shiftX.current = event.clientX - elementRect.left;

    top.current = 'offsetTop' in element ? element.offsetTop : elementRect.top;
    left.current = 'offsetLeft' in element ? element.offsetLeft : elementRect.left;

    if (mode.current === 'translate') {
      translateRectY.current = elementRect.top;
      translateRectX.current = elementRect.left;

      const translate = element.style.transform
        .match(transformTranslateRegExp)?.[0]
        .match(multiNumberRegExp) || [0, 0];

      translateInitX.current = Number(translate[0] || 0);
      prevTransformTranslateX.current = translateInitX.current;

      translateInitY.current = Number(translate[1] || 0);
      prevTransformTranslateY.current = translateInitY.current;

      initWidth.current = elementRect.width;
      initHeight.current = elementRect.height;
    }

    const moveEventName = isMobile ? 'touchmove' : 'mousemove';
    document.addEventListener(moveEventName, move);

    const handleMoveEnd = (moveEndEvent: MouseEvent | TouchEvent) => {
      if (resizeObserver) {
        resizeObserver.disconnect();
      }

      document.removeEventListener(moveEventName, move);

      initHeight.current = -1;
      initWidth.current = -1;

      if (!element) {
        return;
      }

      if (mode.current === 'translate') {
        if (shouldPreventOnClick.current) {
          element.onclick = (clickEvent) => {
            if (isDragging.current) {
              clickEvent.preventDefault();
              clickEvent.stopPropagation();
              isDragging.current = false;
            }
          };
        }

        onMoveEnd.current?.({
          movingCoords: {
            top: top.current,
            bottom: bottom.current,
            left: left.current,
            right: right.current,
          },
          draggableElement: element,
          containerElement: containerRef.current,
          isDragging: isDragging.current,
          event: moveEndEvent,
          isMobile,
        });

        return;
      }

      // Vertical
      const roundedTopOffset = roundToInteger(topOffset.current, gridCellHeight.current, {
        min: gridCellHeight.current,
      });
      const roundedBottomOffset = roundToInteger(bottomOffset.current, gridCellHeight.current, {
        min: gridCellHeight.current,
      });

      const topRemainder = top.current % gridCellHeight.current;
      const topOffsetFromGridLine = min(topRemainder, gridCellHeight.current - topRemainder);

      const bottomRemainder = bottom.current % gridCellHeight.current;
      const bottomOffsetFromGridLine = min(
        bottomRemainder,
        gridCellHeight.current - bottomRemainder
      );

      // RESIZE
      if (sholdResizeByVerticalOnMoveEnd.current) {
        if (top.current < roundedTopOffset && bottom.current < roundedBottomOffset) {
          const cellHeightAfterCorrection =
            parentHeight.current - roundedTopOffset - roundedBottomOffset;

          if (cellHeightAfterCorrection < gridCellHeight.current) {
            if (topOffsetFromGridLine < bottomOffsetFromGridLine) {
              top.current = roundedTopOffset;
              bottom.current = roundedBottomOffset - gridCellHeight.current;
            } else {
              top.current = roundedTopOffset - gridCellHeight.current;
              bottom.current = roundedBottomOffset;
            }
          } else {
            top.current = roundedTopOffset;
            bottom.current = roundedBottomOffset;
          }
        } else if (bottom.current < roundedBottomOffset) {
          bottom.current = roundedBottomOffset;

          const roundedTop = roundToInteger(top.current, gridCellHeight.current, {
            min: roundedTopOffset,
          });
          const cellHeightAfterCorrection = parentHeight.current - roundedBottomOffset - roundedTop;

          if (cellHeightAfterCorrection < gridCellHeight.current) {
            top.current = roundedTop - gridCellHeight.current;
          } else {
            top.current = roundedTop;
          }
        } else if (top.current < roundedTopOffset) {
          top.current = roundedTopOffset;

          const roundedBottom = roundToInteger(bottom.current, gridCellHeight.current, {
            min: roundedBottomOffset,
          });
          const cellHeightAfterCorrection = parentHeight.current - roundedTopOffset - roundedBottom;

          if (cellHeightAfterCorrection < gridCellHeight.current) {
            bottom.current = roundedBottom - gridCellHeight.current;
          } else {
            bottom.current = roundedBottom;
          }
        } else {
          const roundedHeight = roundToInteger(
            parentHeight.current - top.current - bottom.current,
            gridCellHeight.current,
            {
              max: parentHeight.current - roundedTopOffset - roundedBottomOffset,
              min: gridCellHeight.current,
            }
          );

          if (topOffsetFromGridLine < bottomOffsetFromGridLine) {
            top.current = roundToInteger(top.current, gridCellHeight.current, {
              min: roundedTopOffset,
            });
            bottom.current = parentHeight.current - top.current - roundedHeight;
          } else {
            bottom.current = roundToInteger(bottom.current, gridCellHeight.current, {
              min: roundedBottomOffset,
            });
            top.current = parentHeight.current - bottom.current - roundedHeight;
          }
        }
      }
      // MOVE
      else {
        const height = parentHeight.current - top.current - bottom.current;
        if (top.current < roundedTopOffset && bottom.current < roundedBottomOffset) {
          if (topOffsetFromGridLine < bottomOffsetFromGridLine) {
            top.current = roundedTopOffset;
            bottom.current = parentHeight.current - top.current - height;
          } else {
            bottom.current = roundedBottomOffset;
            top.current = parentHeight.current - bottom.current - height;
          }
        } else if (bottom.current < roundedBottomOffset) {
          bottom.current = roundedBottomOffset;
          top.current = parentHeight.current - bottom.current - height;
        } else if (top.current < roundedTopOffset) {
          top.current = roundedTopOffset;
          bottom.current = parentHeight.current - top.current - height;
        } else {
          if (topOffsetFromGridLine < bottomOffsetFromGridLine) {
            const roundedTop = roundToInteger(top.current, gridCellHeight.current, {
              min: roundedTopOffset,
            });

            top.current = roundedTop;
            bottom.current = parentHeight.current - top.current - height;
          } else {
            const roundedBottom = roundToInteger(bottom.current, gridCellHeight.current, {
              min: roundedBottomOffset,
            });

            bottom.current = roundedBottom;
            top.current = parentHeight.current - bottom.current - height;
          }
        }
      }

      // Horizontal
      const roundedLeftOffset = roundToInteger(leftOffset.current, gridCellWidth.current, {
        min: gridCellWidth.current,
      });
      const roundedRightOffset = roundToInteger(rightOffset.current, gridCellWidth.current, {
        min: gridCellWidth.current,
      });

      const leftRemainder = left.current % gridCellWidth.current;
      const leftOffsetFromGridLine = min(leftRemainder, gridCellWidth.current - leftRemainder);

      const rightRemainder = right.current % gridCellWidth.current;
      const rightOffsetFromGridLine = min(rightRemainder, gridCellWidth.current - rightRemainder);

      // RESIZE
      if (sholdResizeByHorizontalOnMoveEnd.current) {
        if (left.current < roundedLeftOffset && right.current < roundedRightOffset) {
          const cellWidthAfterCorrection =
            parentWidth.current - roundedLeftOffset - roundedRightOffset;

          if (cellWidthAfterCorrection < gridCellWidth.current) {
            if (leftOffsetFromGridLine < rightOffsetFromGridLine) {
              left.current = roundedLeftOffset;
              right.current = roundedRightOffset - gridCellWidth.current;
            } else {
              left.current = roundedLeftOffset - gridCellWidth.current;
              right.current = roundedRightOffset;
            }
          } else {
            left.current = roundedLeftOffset;
            right.current = roundedRightOffset;
          }
        } else if (right.current < roundedRightOffset) {
          right.current = roundedRightOffset;

          const roundedLeft = roundToInteger(left.current, gridCellWidth.current, {
            min: roundedLeftOffset,
          });
          const cellWidthAfterCorrection = parentWidth.current - roundedRightOffset - roundedLeft;

          if (cellWidthAfterCorrection < gridCellWidth.current) {
            left.current = roundedLeft - gridCellWidth.current;
          } else {
            left.current = roundedLeft;
          }
        } else if (left.current < roundedLeftOffset) {
          left.current = roundedLeftOffset;

          const roundedRight = roundToInteger(right.current, gridCellWidth.current, {
            min: roundedRightOffset,
          });
          const cellWidthAfterCorrection = parentWidth.current - roundedLeftOffset - roundedRight;

          if (cellWidthAfterCorrection < gridCellWidth.current) {
            right.current = roundedRight - gridCellWidth.current;
          } else {
            right.current = roundedRight;
          }
        } else {
          const roundedWidth = roundToInteger(
            parentWidth.current - left.current - right.current,
            gridCellWidth.current,
            {
              max: parentWidth.current - roundedLeftOffset - roundedRightOffset,
              min: gridCellWidth.current,
            }
          );

          if (leftOffsetFromGridLine < rightOffsetFromGridLine) {
            left.current = roundToInteger(left.current, gridCellWidth.current, {
              min: roundedLeftOffset,
            });
            right.current = parentWidth.current - left.current - roundedWidth;
          } else {
            right.current = roundToInteger(right.current, gridCellWidth.current, {
              min: roundedRightOffset,
            });
            left.current = parentWidth.current - right.current - roundedWidth;
          }
        }
      }
      // MOVE
      else {
        const width = parentWidth.current - left.current - right.current;
        if (left.current < roundedLeftOffset && right.current < roundedRightOffset) {
          if (leftOffsetFromGridLine < rightOffsetFromGridLine) {
            left.current = roundedLeftOffset;
            right.current = parentWidth.current - left.current - width;
          } else {
            right.current = roundedRightOffset;
            left.current = parentWidth.current - right.current - width;
          }
        } else if (right.current < roundedRightOffset) {
          right.current = roundedRightOffset;
          left.current = parentWidth.current - right.current - width;
        } else if (left.current < roundedLeftOffset) {
          left.current = roundedLeftOffset;
          right.current = parentWidth.current - left.current - width;
        } else {
          if (leftOffsetFromGridLine < rightOffsetFromGridLine) {
            const roundedLeft = roundToInteger(left.current, gridCellWidth.current, {
              min: roundedLeftOffset,
            });

            left.current = roundedLeft;
            right.current = parentWidth.current - left.current - width;
          } else {
            const roundedRight = roundToInteger(right.current, gridCellWidth.current, {
              min: roundedRightOffset,
            });

            right.current = roundedRight;
            left.current = parentWidth.current - right.current - width;
          }
        }
      }

      if (
        !onMoveEnd.current?.({
          movingCoords: {
            top: top.current,
            bottom: bottom.current,
            left: left.current,
            right: right.current,
          },
          draggableElement: element,
          containerElement: containerRef.current,
          isDragging: isDragging.current,
          event: moveEndEvent,
          isMobile,
        })
      ) {
        const prevDisplay = element.style.display;
        element.style.display = 'none';
        element.style.top = `${top.current}px`;
        element.style.bottom = `${bottom.current}px`;
        element.style.left = `${left.current}px`;
        element.style.right = `${right.current}px`;
        element.style.display = prevDisplay;
      }
    };

    const moveEndEventName = isMobile ? 'touchend' : 'mouseup';

    document.addEventListener(moveEndEventName, handleMoveEnd, { once: true });
  });
};
