import { useEffect, useMemo, useRef, useState } from 'react';

import { useMove } from './useMove/useMove';
import { useOnDragStart } from './useOnDragStart';

export interface AbsolutePositionProps {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}

export interface OnMoveParams {
  draggableElement: HTMLElement | SVGElement | null;
  containerElement: HTMLElement | SVGElement | null;
  movingCoords: AbsolutePositionProps;
  event: MouseEvent | TouchEvent;
  isDragging: boolean;
  isMobile: boolean;
}

export type AllowedDirection = 'top' | 'bottom' | 'left' | 'right';

export type ElementPosition = 'absolute' | 'fixed';

export type DragMode = 'move' | 'resize' | 'translate';

export interface UseDnRParams {
  draggableElement?: HTMLElement | null;
  containerElement?: HTMLElement | null;
  topOffset?: number;
  bottomOffset?: number;
  leftOffset?: number;
  rightOffset?: number;
  cellsCountX?: number;
  cellsCountY?: number;
  noRestrictions?: boolean;
  allowedDirections?: AllowedDirection[];
  mode?: DragMode;
  minWidth?: number;
  minHeight?: number;
  position?: ElementPosition;
  shouldMemoizeParams?: boolean;
  sholdResizeByVerticalOnMoveStart?: boolean;
  sholdResizeByHorizontalOnMoveStart?: boolean;
  sholdResizeByVerticalOnMoveEnd?: boolean;
  sholdResizeByHorizontalOnMoveEnd?: boolean;
  shouldPreventOnClick?: boolean;
  onMoveStart?: (params: OnMoveParams) => boolean;
  onMoving?: (params: OnMoveParams) => boolean;
  onMoveEnd?: (params: OnMoveParams) => boolean;
}

export const transformTranslateRegExp = /translate\(.+?\)/;
export const multiNumberRegExp = new RegExp('-?\\d+\\.?\\d*', 'g');

export const useDnR = (params: UseDnRParams = {}) => {
  const isMobile = useRef(
    'ontouchstart' in window ||
      navigator.maxTouchPoints > 0 ||
      (navigator as any).msMaxTouchPoints > 0
  ).current;

  const { draggableElement, containerElement = null, shouldMemoizeParams = false } = params;

  const isDragging = useRef(false);

  // Shift
  // ? the shifts should be changed only on drag start event
  const shiftY = useRef(0);
  const shiftX = useRef(0);

  const top = useRef(0);
  const bottom = useRef(0);
  const left = useRef(0);
  const right = useRef(0);

  const gridCellWidth = useRef(1);
  const gridCellHeight = useRef(1);

  const initHeight = useRef(-1);
  const initWidth = useRef(-1);

  // Elements
  const [element, setElement] = useState<HTMLElement | SVGElement | null>(draggableElement || null);
  const containerRef = useRef<HTMLElement | null>(null);

  // Parent sizes
  // ? the parent sizes should be changed only on drag start event
  const parentWidth = useRef(0);
  const parentHeight = useRef(0);

  // Directions flags
  const hasTopDirection = useRef(false);
  const hasBottomDirection = useRef(false);
  const hasLeftDirection = useRef(false);
  const hasRightDirection = useRef(false);

  // Mode
  const mode = useRef<DragMode>('translate');

  // Offsets
  const topOffset = useRef(0);
  const bottomOffset = useRef(0);
  const leftOffset = useRef(0);
  const rightOffset = useRef(0);

  // Grid cells count
  const gridCellsCountX = useRef(0);
  const gridCellsCountY = useRef(0);

  // Position
  const position = useRef<ElementPosition>('absolute');

  // Width and height limits
  const minWidth = useRef(10);
  const minHeight = useRef(10);

  // Should resize
  const sholdResizeByVerticalOnMoveStart = useRef(true);
  const sholdResizeByHorizontalOnMoveStart = useRef(true);
  const sholdResizeByVerticalOnMoveEnd = useRef(true);
  const sholdResizeByHorizontalOnMoveEnd = useRef(true);

  // Parameters
  const memoizedParams = useRef(params);
  const parametersDependency = shouldMemoizeParams ? memoizedParams.current : params;

  // Move hanlders
  const onMoveStart = useRef(parametersDependency.onMoveStart);
  const onMoving = useRef(parametersDependency.onMoving);
  const onMoveEnd = useRef(parametersDependency.onMoveEnd);

  // Props for transform translate
  const translateInitX = useRef(0);
  const translateInitY = useRef(0);
  const translateRectX = useRef(0);
  const translateRectY = useRef(0);
  const prevTransformTranslateX = useRef(0);
  const prevTransformTranslateY = useRef(0);

  const shouldPreventOnClick = useRef(false);

  useEffect(() => {
    const allowedDirections = parametersDependency.allowedDirections?.length
      ? parametersDependency.allowedDirections
      : ['top', 'bottom', 'left', 'right'];

    hasTopDirection.current = allowedDirections.includes('top');
    hasBottomDirection.current = allowedDirections.includes('bottom');
    hasLeftDirection.current = allowedDirections.includes('left');
    hasRightDirection.current = allowedDirections.includes('right');

    mode.current = parametersDependency.mode ?? 'move';

    topOffset.current = parametersDependency.topOffset ?? 0;
    bottomOffset.current = parametersDependency.bottomOffset ?? 0;
    leftOffset.current = parametersDependency.leftOffset ?? 0;
    rightOffset.current = parametersDependency.rightOffset ?? 0;

    gridCellsCountX.current = parametersDependency.cellsCountX ?? 0;
    gridCellsCountY.current = parametersDependency.cellsCountY ?? 0;

    position.current = parametersDependency.position ?? 'absolute';

    minWidth.current = parametersDependency.minWidth ?? 10;
    minHeight.current = parametersDependency.minHeight ?? 10;

    sholdResizeByVerticalOnMoveStart.current =
      parametersDependency.sholdResizeByVerticalOnMoveStart ?? false;
    sholdResizeByHorizontalOnMoveStart.current =
      parametersDependency.sholdResizeByHorizontalOnMoveStart ?? false;

    sholdResizeByVerticalOnMoveEnd.current =
      parametersDependency.sholdResizeByVerticalOnMoveEnd ?? false;
    sholdResizeByHorizontalOnMoveEnd.current =
      parametersDependency.sholdResizeByHorizontalOnMoveEnd ?? false;

    shouldPreventOnClick.current = parametersDependency.shouldPreventOnClick ?? false;

    onMoveStart.current = parametersDependency.onMoveStart;
    onMoving.current = parametersDependency.onMoving;
    onMoveEnd.current = parametersDependency.onMoveEnd;
  }, [parametersDependency]);

  const move = useMove({
    bottom,
    bottomOffset,
    containerRef,
    element,
    hasBottomDirection,
    hasLeftDirection,
    hasRightDirection,
    hasTopDirection,
    initHeight,
    initWidth,
    isDragging,
    isMobile,
    left,
    leftOffset,
    minHeight,
    minWidth,
    mode,
    onMoving,
    parentHeight,
    parentWidth,
    prevTransformTranslateX,
    prevTransformTranslateY,
    right,
    rightOffset,
    shiftX,
    shiftY,
    top,
    topOffset,
    translateInitX,
    translateInitY,
    translateRectX,
    translateRectY,
  });

  const onDragStart = useOnDragStart({
    bottom,
    bottomOffset,
    containerRef,
    element,
    gridCellHeight,
    gridCellsCountX,
    gridCellsCountY,
    gridCellWidth,
    initHeight,
    initWidth,
    isDragging,
    isMobile,
    left,
    leftOffset,
    mode,
    move,
    onMoveEnd,
    onMoveStart,
    parentHeight,
    parentWidth,
    position,
    prevTransformTranslateX,
    prevTransformTranslateY,
    right,
    rightOffset,
    shiftX,
    shiftY,
    sholdResizeByHorizontalOnMoveEnd,
    sholdResizeByVerticalOnMoveEnd,
    shouldPreventOnClick,
    top,
    topOffset,
    translateInitX,
    translateInitY,
    translateRectX,
    translateRectY,
    containerElement,
  });

  useEffect(() => {
    const eventName = isMobile ? 'touchstart' : 'mousedown';
    if (!element) {
      return;
    }

    element.addEventListener(eventName, onDragStart as EventListenerOrEventListenerObject);

    return () =>
      element.removeEventListener(eventName, onDragStart as EventListenerOrEventListenerObject);
  }, [element, isMobile, onDragStart]);

  useEffect(() => {
    setElement((prev) => {
      if (draggableElement !== undefined) {
        return draggableElement;
      }

      return prev;
    });
  }, [draggableElement]);

  const topCoord = top.current;
  const bottomCoord = bottom.current;
  const leftCoord = left.current;
  const rightCoord = right.current;

  const coords = useMemo(
    () => ({
      top: topCoord,
      bottom: bottomCoord,
      left: leftCoord,
      right: rightCoord,
    }),
    [topCoord, bottomCoord, leftCoord, rightCoord]
  );

  return {
    coords,
    isMobile,
    isDragging: isDragging.current,
    draggaleElement: element,
    setDraggaleElement: setElement,
  };
};
